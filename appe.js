var express = require('express');
var userFile = require('./users.json');
var bodyParser = require('body-parser');

var app = express();
var port = process.env.PORT || 3000;

var URLbase = "/apiperu/v1/";


//get con query string
// llamada http://localhost:3000/apiperu/v1/users/23/22?name=Jaime&last=llarena
app.get(URLbase + 'users/:id/:id2',
  function(req, res){
    console.log(req.query);
    console-log(req.params);
  });

//GET con query string
// llamada http://localhost:3000/apiperu/v1/users?id=23
app.get(URLbase + 'users',
  function(req, res){
    console.log(req.query);
    console.log(req.query.id);
  });

app.use(bodyParser.json());
// GET users
app.get(URLbase + "users",
  function(req, res){
    console.log("GET /apiperu/v1/users");
    //res.status(200);
    //res.send({"msg":"respuesta correcta"});
    res.send(userFile);
  });

app.get('/', function (req, res) {
  res.send('Hola Lima Peru- jaime!');
});

// GET users por ID
app.get(URLbase + 'users/:id',
  function(req, res){
    console.log('GET / apiperu/v1/users/:id');
    console.log(req.params);
    console.log(req.params.id);
    let pos = req.params.id;
    let response = (userFile[pos-1]== undefined) ? {"msg":"No existe"} : userFile[pos-1];
    res.send(response);
  });

  //Ejemplo: PUT http://localhost:8080/items
  app.put(URLbase + 'users',
    function(req, res) {
      //var itemId = req.body.id;
      //var data = req.body.data;
  //res.send('Update ' + itemId + ' with ' + data);
      res.send('Hola Carolina!');
  });

  // Post users
  app.post(URLbase + 'users',
    function(req, res) {
      console.log("POST /apiperu/v1/users");
      let newID = userFile.length + 1;
      let newUser = {
        "userID" : newID,
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email,
        "password" : req.body.password
      }
      userFile.push(newUser);
      console.log(userFile);
      res.status(201);
      res.send({"msg": "Usuario añadido correctamente", newUser})
    });

  // Borrar usuarios
  //Ejemplo: DELETE http://localhost:8080/items

app.listen(port);
