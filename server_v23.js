var express = require('express');
var userFile = require('./users.json');
var bodyParser = require('body-parser');
var requestJSON = require('request-json');

// variable marco de aplicacion web + puerto
var app = express();
var port = process.env.PORT || 3000;

var URLbase = "/apiperu/v1/";

//var baseMlabURL = "http://localhost:3000/apiperu/v1/";
// 05-11-2018

var baseMLabURL = "https://api.mlab.com/api/1/databases/apiperu-jmcc/collections/";

var apikeyMLab = "7Lgmp3HkdHo7T6AW0hspJSn8ddeUONkv"; //7Lgmp3HkdHo7T6AW0hspJSn8ddeUONkv

console.log("hola perù 2 ");

//valores  de mlab JULIO TORRES
//var urlMlabRaiz = "https://api.mlab.com/api/1/databases/apiperudb/collections/";
//var apiKey = "apiKey=pQEEf1Rpc-hMSphynoH-9OfSEo6IupGr";
//var clienteMlab
// http://localhost:3000/apiperu/v1/users
// Clase 02-11-18
//PUT of user
app.put(URLbase + 'users/:id',
function(req, res) {
  var id = req.params.id;
  var queryStringID = 'q={"id":' + id + '}&';
  var clienteMlab = requestJSON.createClient(baseMLabURL);
  console.log("AAA: " + clienteMlab);
  clienteMlab.get('user?'+ queryStringID + apikeyMLab ,
    function(error, respuestaMLab , body) {
     var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
     clienteMlab.put(baseMLabURL + 'user?q={"id": ' + id + '}&' + apikeyMLab, JSON.parse(cambio),
      function(error, respuestaMLab, body) {
        console.log("body:"+ body);
       res.send(body);
      });
 });
});

//DELETE user with id
app.delete(URLbase + "users/:id",
  function(req, res){
    console.log("entra al DELETE");
    console.log("request.params.id: " + req.params.id);
    var id = req.params.id;
    var queryStringID = 'q={"id":' + id + '}&';
    console.log(baseMLabURL + 'user?' + queryStringID + apikeyMLab);
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' +  queryStringID + apikeyMLab,
      function(error, respuestaMLab, body){
        var respuesta = body[0];
        console.log("body delete:"+ respuesta);
        httpClient.delete(baseMLabURL + "user/" + respuesta._id.$oid +'?'+ apikeyMLab,
          function(error, respuestaMLab,body){
            res.send(body);
        });
      });
  });

//Esta vaaa --- inicia con la version api Mlab
app.get(URLbase + 'users',
  function(req, res){
    console.log("GET /colapi/");
    console.log(req.query.id);
  });
///// temrina con la version api Mlab

//get con query string
// llamada http://localhost:3000/apiperu/v1/users/23/22?name=Jaime&last=llarena
app.get(URLbase + 'users/:id/:id2',
  function(req, res){
    console.log(req.query);
    console-log(req.params);
  });

//GET con query string
// llamada http://localhost:3000/apiperu/v1/users?id=23
app.get(URLbase + 'users',
  function(req, res){
    console.log(req.query);
    console.log(req.query.id);
  });

app.use(bodyParser.json());
// GET users
app.get(URLbase + "users",
  function(req, res){
    console.log("GET /apiperu/v1/users");
    //res.status(200);
    //res.send({"msg":"respuesta correcta"});
    res.send(userFile);
  });

app.get('/', function (req, res) {
  res.send('Hola Lima Peru- jaime!');
});

// GET users por ID
app.get(URLbase + 'users/:id',
  function(req, res){
    console.log('GET / apiperu/v1/users/:id');
    console.log(req.params);
    console.log(req.params.id);
    let pos = req.params.id;
    let response = (userFile[pos-1]== undefined) ? {"msg":"No existe"} : userFile[pos-1];
    res.send(response);
  });

  //Ejemplo: PUT http://localhost:8080/items
  app.put(URLbase + 'users',
    function(req, res) {
      //var itemId = req.body.id;
      //var data = req.body.data;
  //res.send('Update ' + itemId + ' with ' + data);
      res.send('Hola Carolina!');
  });

  // Post users
  app.post(URLbase + 'users',
    function(req, res) {
      console.log("POST /apiperu/v1/users");
      let newID = userFile.length + 1;
      let newUser = {
        "userID" : newID,
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email,
        "password" : req.body.password
      }
      userFile.push(newUser);
      console.log(userFile);
      res.status(201);
      res.send({"msg": "Usuario añadido correctamente", newUser})
    });

  // Borrar usuarios
  //Ejemplo: DELETE http://localhost:8080/items

app.listen(port);
