# Image docker base inicial
FROM node:latest

# Crear directorio de trabajo del contenedor
WORKDIR /docker-api

# Copiar archivos del proyecto en directorio de Docker
ADD ./docker-api

# Instalar las dependencias del proyecto en producción.
# RUN npm install --production
#                 --only= production
# Puerto donde exponemos nuestro contenedor (el mismo que definimos en nuestra API)
EXPOSE  3000

# Lanzar la aplicacion (appe.js)
# CMD ["npm", "run", "start"]
CMD ["npm", "start"]

#en la terminal validar
################
#sudo docker build -t imagen-peru.
# sudo docker build -t imagen-peru

# lanzar el contenedor
#sudo docker run imagen-peru


#Hay que mapear el puerto de la maquina virtual con el contenedor...
# sudo docker run -p 3000_3000 imagen-peru
#se lanza y se prueba en postman

# sudo docker stop

#sudo docker ps

#& .-  permite ejecutarlo en segundo plano

#sudo docker run --name apli -p 3000:3000 imagen-peru &
#.....

#sudo docker -rm apli
